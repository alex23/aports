# Contributor: Michael Mason <ms13sp@gmail.com>
# Maintainer: Celeste <cielesti@protonmail.com>
pkgname=freetds
pkgver=1.4.14
pkgrel=0
pkgdesc="Tabular Datastream Library"
url="https://www.freetds.org/"
arch="all"
license="GPL-2.0-or-later OR LGPL-2.0-or-later"
makedepends="openssl-dev>3 linux-headers readline-dev unixodbc-dev"
subpackages="$pkgname-static $pkgname-dev $pkgname-doc"
source="https://www.freetds.org/files/stable/freetds-$pkgver.tar.bz2"
options="!check"  # tests require running SQL server http://www.freetds.org/userguide/confirminstall.htm#TESTS

prepare() {
 	default_prepare
	update_config_sub
}

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--infodir=/usr/share/info \
		--enable-msdblib \
		--with-openssl=/usr \
		--enable-odbc \
		--with-unixodbc=/usr
	make
}

check() {
	cd "$builddir"/src/replacements
	make check
}

package() {
	make -j1 DESTDIR="$pkgdir" install
}

sha512sums="
ae1f5df0084eeff8c74e9007925d5c631ba8e869c08768829b47c657724eb6aa13da20ee783cc81bd1d5a7700a007d5c9a99c02ca38cd174afc1a7d3987b4a61  freetds-1.4.14.tar.bz2
"
