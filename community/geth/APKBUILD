# Contributor: André Klitzing <aklitzing@gmail.com>
# Maintainer: André Klitzing <aklitzing@gmail.com>
pkgname=geth
pkgver=1.14.3
pkgrel=1
pkgdesc="Official Go implementation of the Ethereum protocol"
url="https://geth.ethereum.org/"
arch="all"
license="LGPL-3.0-or-later"
makedepends="go linux-headers"
checkdepends="fuse"
options="!check chmod-clean net"
source="$pkgname-$pkgver.tar.gz::https://github.com/ethereum/go-ethereum/archive/v$pkgver.tar.gz"
builddir="$srcdir/go-ethereum-$pkgver"

# secfixes:
#   1.10.22-r0:
#     - CVE-2022-37450

export GOROOT=/usr/lib/go
export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"

build() {
	make
}

check() {
	make test
}

package() {
	mkdir -p "$pkgdir"/usr/bin
	install -m755 -t "$pkgdir"/usr/bin build/bin/*
}

sha512sums="
bb59e375ab0b6d52d4f0034d6c1e992c8046e6a18168cd6ab93bcc60e45e69c87660b977c481824e700072871116bb5fe03f07aa0f6c9ae7e336cd76b8665545  geth-1.14.3.tar.gz
"
