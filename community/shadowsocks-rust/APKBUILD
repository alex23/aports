# Maintainer: Celeste <cielesti@protonmail.com>
pkgname=shadowsocks-rust
pkgver=1.18.4
pkgrel=0
pkgdesc="Rust port of shadowsocks"
url="https://github.com/shadowsocks/shadowsocks-rust"
arch="all"
license="MIT"
makedepends="
	cargo
	mimalloc2-dev
	openssl-dev
	"
subpackages="
	$pkgname-sslocal
	$pkgname-ssmanager
	$pkgname-ssserver
	$pkgname-ssservice
	$pkgname-ssurl
	"
source="$pkgname-$pkgver.tar.gz::https://github.com/shadowsocks/shadowsocks-rust/archive/refs/tags/v$pkgver.tar.gz"
options="net !check" # fail for some reason

prepare() {
	default_prepare

	# Rust target triple.
	local target=$(rustc -vV | sed -n 's/host: //p')

	# Build against system-provided libs
	mkdir -p .cargo
	cat >> .cargo/config.toml <<-EOF
		[target.$target]
		mimalloc = { rustc-link-lib = ["mimalloc"] }
	EOF

	cargo fetch --target="$CTARGET" --locked
}

build() {
	local features="
		aead-cipher-2022
		local-http-native-tls
		local-redir
		local-socks4
		local-tunnel
		logging
		manager
		mimalloc
		multi-threaded
		server
		service
		utility
		"
	case "$CARCH" in
	ppc64le|s390x|riscv64|loongarch64)
		;;
	*)
		# ioctl-sys
		features="$features local-tun"
		;;
	esac
	cargo build --release \
		--frozen \
		--bins \
		--no-default-features \
		--features="$(echo $features | tr -s " " ",")"
}

check() {
	cargo test --frozen
}

package() {
	depends="
		$pkgname-sslocal=$pkgver-r$pkgrel
		$pkgname-ssmanager=$pkgver-r$pkgrel
		$pkgname-ssserver=$pkgver-r$pkgrel
		$pkgname-ssservice=$pkgver-r$pkgrel
		$pkgname-ssurl=$pkgver-r$pkgrel
		"
	cd target/release
	install -Dm755 -t "$pkgdir"/usr/bin/ \
		sslocal \
		ssmanager \
		ssserver \
		ssservice \
		ssurl
}

sslocal() {
	pkgdesc="$pkgdesc (sslocal binary)"

	amove usr/bin/sslocal
}

ssmanager() {
	pkgdesc="$pkgdesc (ssmanager binary)"

	amove usr/bin/ssmanager
}

ssserver() {
	pkgdesc="$pkgdesc (ssserver binary)"

	amove usr/bin/ssserver
}

ssservice() {
	pkgdesc="$pkgdesc (ssservice binary)"

	amove usr/bin/ssservice
}

ssurl() {
	pkgdesc="$pkgdesc (ssurl binary)"

	amove usr/bin/ssurl
}

sha512sums="
82e0a08d7bdcd863bbe98d3ea4369cae5540c2ce2c80d7829897862886f3b12b839b4986373ff208b8d98e01bfa1d4b07f39ab9e5400389fb362953d00224d9c  shadowsocks-rust-1.18.4.tar.gz
"
